package condominio;

public class Cadastro {

	String titularAp;
	int numeroAp;
	int qtdadeMoradores;
	String tipo; // alugado ou comprado
	boolean pagamentoCondominio;
	int advertencias;
	String alineMusa;

	public void consultaAndar(int numAp, String donoAp) {
		char[] digitos = String.valueOf(numAp).toCharArray();

		if (digitos.length == 2) {
			System.out.println("O andar do Apartamento do(a) titular " + donoAp + " � " + digitos[0]);
		} else {
			System.out.println("O andar do Apartamento do(a) titular " + donoAp + " � " + digitos[0] + digitos[1]);
		}
	}

	public void atrasosAdvt(boolean pago) {
		int valorUnitario = 350;
		if (pago && this.advertencias == 0) {
			System.out.println("Seu apartamento n�o possui pendencias com condominio e advert�ncias!!");
		} else if (pago && this.advertencias > 0) {
			this.advertencias *= 100;
			System.out.println(
					"Voce n�o tem atrasos, mas devido a suas advertencias, tem de pagar R$ " + this.advertencias);
		}

		else if (!pago && this.advertencias > 0) {
			valorUnitario*=2;
			float valorTotal = valorUnitario * (this.advertencias);
			System.out.println("Voce tem de pagar R$ " + valorTotal);

		} else if (!pago && this.advertencias == 0) {
			System.out.println("Voce tem de pagar R$ " + valorUnitario);
		}

	}
	
}