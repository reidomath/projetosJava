
public class ProgramaPrincipal {
	public static void main(String[] args) {
		
		// Inicializa a instancia com valorA=2 e valorB=4 encapsulado na classe NumeroComplexo - Construtor
		NumeroComplexo numeroComplexo = new NumeroComplexo();
		
		// Inicializa a isntancia com valor preenchido como parametro
		NumeroComplexo numeroComplexo1 =  new NumeroComplexo(2, 4);
		NumeroComplexo numeroComplexo2 =  new NumeroComplexo(1, 5);
		NumeroComplexo numeroComplexo3 =  new NumeroComplexo(9, 6);
		NumeroComplexo numeroComplexo4 =  new NumeroComplexo(8, 8);
		
		// Testando metodo que retorna se os numeros compelxos s�o iguais
		System.out.println(numeroComplexo.ehIgual(numeroComplexo1));
		System.out.println(numeroComplexo.ehIgual(numeroComplexo2));
		System.out.println(numeroComplexo.ehIgual(numeroComplexo3));
		
		// Testando metodo que imprime o numero complexo
		System.out.println(numeroComplexo1.imprimeNumero());
		System.out.println(numeroComplexo4.imprimeNumero());
		
		// Testando metodo que soma os numeros complexos
		System.out.println(numeroComplexo.soma(numeroComplexo2));
		System.out.println(numeroComplexo1.soma(numeroComplexo4));
		
		// Testando metodo que subtrai os numeros complexos
		System.out.println(numeroComplexo.subtrai(numeroComplexo2));
		System.out.println(numeroComplexo3.subtrai(numeroComplexo4));
		
		// Testando metodo que multiplica os numeros complexos
		System.out.println(numeroComplexo1.multiplica(numeroComplexo4));
		System.out.println(numeroComplexo.multiplica(numeroComplexo1));
		System.out.println(numeroComplexo3.multiplica(numeroComplexo4));
		
		// Testando metodo que divide os numeros complexos
		System.out.println(numeroComplexo3.divide(numeroComplexo1));
		System.out.println(numeroComplexo.divide(numeroComplexo3));
		
		
		
		
	}

}
