import java.text.DecimalFormat;

public class NumeroComplexo {
	private double valorA;
	private double valorB;

	public NumeroComplexo() {
		this.valorA = 2;
		this.valorB = 4;
	}

	public NumeroComplexo(double valorA, double valorB) {
		this.valorA = valorA;
		this.valorB = valorB;
	}

	public void inicializaNumero(double valorA, double valorB) {
		this.valorA = valorA;
		this.valorB = valorB;
	}

	public String imprimeNumero() {
		System.out.print("Imprime numero complexo: ");
		return "(" + this.valorA + ")" + " + " + "(" + this.valorB + ")" + "i";
	}

	public boolean ehIgual(NumeroComplexo numeroComplexo) {
		System.out.print("Verifica se numeros complexos s�o iguais: ");
		if (numeroComplexo.valorA == this.valorA && numeroComplexo.valorB == this.valorB) {
			return true;
		}
		return false;
	}

	public String soma(NumeroComplexo numeroComplexo) {
		System.out.print("Soma os numeros complexos: ");
		double aux1, aux2;
		aux1 = this.valorA + numeroComplexo.valorA;
		aux2 = this.valorB + numeroComplexo.valorB;
		return "(" + aux1 + ")" + " + " + "(" + aux2 + ")" + "i";

	}

	public String subtrai(NumeroComplexo numeroComplexo) {
		System.out.print("Subtrai os numeros complexos: ");
		double aux1, aux2;
		aux1 = this.valorA - numeroComplexo.valorA;
		aux2 = this.valorB - numeroComplexo.valorB;
		return "(" + aux1 + ")" + " + " + "(" + aux2 + ")" + "i";

	}

	public String multiplica(NumeroComplexo numeroComplexo) {
		System.out.print("Multiplica os numeros complexos: ");
		double aux1, aux2, aux3, aux4;
		aux1 = this.valorA * numeroComplexo.valorA;
		aux2 = this.valorB * numeroComplexo.valorB;
		aux1 = aux1 - aux2;

		aux3 = this.valorA * numeroComplexo.valorB;
		aux4 = this.valorB * numeroComplexo.valorA;
		aux3 = aux3 + aux4;
		return "(" + aux1 + ")" + " + " + "(" + aux2 + ")" + "i";
	}

	public String divide(NumeroComplexo numeroComplexo) {
		System.out.print("Divide os numeros complexos: ");
		double aux1, aux2, aux3, aux4;
		aux1 = this.valorA * numeroComplexo.valorA;
		aux2 = this.valorB * numeroComplexo.valorB;
		aux1 = aux1 + aux2;

		aux2 = numeroComplexo.valorA * numeroComplexo.valorA;
		aux3 = numeroComplexo.valorB * numeroComplexo.valorB;
		aux2 = aux2 + aux3;

		aux3 = this.valorB * numeroComplexo.valorA;
		aux4 = this.valorA * numeroComplexo.valorB;
		aux3 = aux3 - aux4;

		aux1 = aux1 / aux2;
		aux3 = aux3 / aux2;

		// Retona o resultado da divis�o, formatando o double com 2 casas decimais ap�s
		// a virgula
		return "(" + String.format("%.2f", aux1) + ")" + " + " + "(" + String.format("%.2f", aux3) + ")" + "i";
	}

}
