package bookcase;

public class PublishingHouse extends Book {

	private String nameHouse;
	public boolean pubHouse(int editionNumber) {
		if (editionNumber > 1950 && editionNumber < 2000) {
			this.nameHouse = "GunsBooks Library of the year - New Work City, EUA";
			return true;
		} else {
			this.nameHouse = "AllBook of the time - London, England";
			return false;
		}
	}
	
	public String getNameHouse() {
		return this.nameHouse;
	}

}
