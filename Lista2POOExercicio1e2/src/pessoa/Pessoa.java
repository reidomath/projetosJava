package pessoa;

public class Pessoa {
	private String nome;
	private String telefone;
	private String email;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		if (nome != "" && nome != null) {
			this.nome = nome;
		} else {
			System.out.println("N�o � possivel declarar uma pessoa com o nome vazio.");
		}
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		if (telefone != "" && telefone != null) {
			this.telefone = telefone;
		} else {
			System.out.println("N�o � possivel declarar uma pessoa com o telefone vazio.");
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email != "" && email != null) {
			this.email = email;
		} else {
			System.out.println("N�o � possivel declarar uma pessoa com o email vazio.");
		}
	}

	public void alteraEmailTelefone(String email, String telefone) {
		setEmail(email);
		setTelefone(telefone);
	}

	public void imprimeDadosPessoas(Pessoa pessoa) {
		if (getNome() != null && getEmail() != null && getTelefone() != null) {
			System.out.println("Nome: " + getNome());
			System.out.println("Email: " + getEmail());
			System.out.println("Telefone: " + getTelefone() + "\n");
		}
	}
}
