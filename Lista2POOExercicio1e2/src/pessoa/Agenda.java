package pessoa;

public class Agenda {

	private Pessoa[] pessoas = new Pessoa[10];

	public boolean addPessoaAgenda(Pessoa pessoa) {
		System.out.println("Adicionando na agenda");
		for (int i = 0; i < pessoas.length; i++) {
			if (pessoas[i] == null) {
				this.pessoas[i] = pessoa;
				return true;
			}
		}
		return false;
	}

	public boolean rmPessoaAgenda(Pessoa pessoa) {
		System.out.println("Removendo na agenda");
		for (int i = 0; i < pessoas.length; i++) {
			if (this.pessoas[i] == pessoa) {
				this.pessoas[i] = null;
				return true;
			}
		}
		return false;
	}

	public int verificaPessoaCadastrada(Pessoa pessoa) {
		System.out.print("Verificando pessoa cadastrada: ");
		for (int i = 0; i < pessoas.length; i++) {
			if (this.pessoas[i] == pessoa) {
				return i;
			}
		}
		return 0;
	}

	public void buscaTelefone(String nome) {
		System.out.print("Busca telefone: ");
		nome = nome.toUpperCase();
		for (int i = 0; i < 2; i++) {
			if (this.pessoas[i].getNome().toUpperCase().contains(nome)) {
				System.out.println(pessoas[i].getTelefone());
			}
		}
	}
	public void buscaEmail(String nome) {
		System.out.print("Busca email: ");
		nome = nome.toUpperCase();
		for (int i = 0; i < 2; i++) {
			if (this.pessoas[i].getNome().toUpperCase().contains(nome)) {
				System.out.println(pessoas[i].getEmail());
			}
		}
	}
	
	public void imprimeDados(Pessoa pessoa) {
		System.out.println("\nImprime dados:");
		pessoa.imprimeDadosPessoas(pessoa);
	}

}
