
public class TestaMetodos {
	public static void main(String[] args) {

		Conta contaMath = new Conta(259, 72049);
		
		System.out.println(contaMath.getAgencia());
		
		contaMath.getCliente().setIdade(23);
		
		System.out.println(contaMath.getCliente().getIdade());
		
		contaMath.getEndereco().setRua("Rua speers");
		
		System.out.println(contaMath.getEndereco().getRua());
		
		Conta contaSumio = new Conta(209, 49903);
		Conta contaThassius = new Conta(223, 34566);
		Conta contaMiron = new Conta(329, 89434);
		
		System.out.println(Conta.getTotal());
		
		Conta contaTesteContrutor = new Conta(773);
		
		System.out.println(contaTesteContrutor.getConta());
		
		
		int total = Conta.getTotal();
		System.out.println("Foram criadas "+ total + " contas!");
	}
}
