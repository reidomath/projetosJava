
public class Endereco {

	private String rua;
	private int numeroRua;
	private String cep;
	
	public void setRua(String rua) {
		this.rua=rua;
	}
	public String getRua() {
		return this.rua;
	}
	
	public void setNumeroRua(int numeroRua) {
		this.numeroRua= numeroRua;
	}
	public int getNumeroRua() {
		return this.numeroRua;
	}
	
	public void setCep(String cep) {
		this.cep=cep;
	}
	public String getCep() {
		return this.cep;
	}
	
}
