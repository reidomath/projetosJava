
public class Conta {

	private int agencia;
	private int conta;
	private Cliente cliente = new Cliente();
	private Endereco endereco = new Endereco();

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public int getAgencia() {
		return this.agencia;
	}
	
	public void setConta(int conta) {
		this.conta = conta;
	}
	public int getConta() {
		return this.conta;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente=cliente;
	}
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Endereco getEndereco() {
		return this.endereco;
	}

	
}
