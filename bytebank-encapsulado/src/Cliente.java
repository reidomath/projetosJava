
public class Cliente {

	private String titular;
	private int idade;
	
	public void setTitular(String titular) {
		this.titular= titular;
	}
	public String getTitular() {
		return this.titular;
	}
	
	public void setIdade(int idade) {
		this.idade=idade;
	}
	public int getIdade() {
		return this.idade;
	}
}
