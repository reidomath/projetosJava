package bytebank;

public class Conta {
	float saldo;
	int agencia, numeroConta;
	String nomeTitular, cidade, endereco;
	
	public void deposita(double valor) {
		this.saldo +=valor;
	}
	
	public boolean saca(double valor) {
		if (this.saldo>= valor) {
			this.saldo-=valor;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean transfere (double valor, Conta destino) {
		if (this.saldo>= valor) {
			this.saldo-=valor;
			destino.saldo+=valor;
			return true;
		} else {
			return false;
		}
	}
	
	
}
