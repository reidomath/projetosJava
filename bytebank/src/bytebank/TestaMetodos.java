package bytebank;

public class TestaMetodos {
	public static void main(String[] args) {

		Conta contaMath = new Conta();
		Conta contaMiron = new Conta();
		contaMiron.saldo = 5600;
		
		contaMath.deposita(700);
		System.out.println("Foi depositado R$ " +contaMath.saldo+ " na conta do Math.");
		
		boolean sacaSucesso = contaMath.saca(20);
		if (sacaSucesso) {
			System.out.println("\nSaque realizado com sucesso!");
		} else {
			System.out.println("Saque n�o p�de ser realizado. Dinheiro insuficiente!");
		}
		System.out.println("Saldo da conta Math: R$ "+ contaMath.saldo);
		
		boolean transfSucesso = contaMath.transfere(300, contaMiron);
		
		if (transfSucesso) {
			System.out.println("\nTransferencia realizada com sucesso");
			System.out.println("Saldo Math: "+ contaMath.saldo);
			System.out.println("Saldo Miron: "+ contaMiron.saldo);
		}
		
	}
}
