
public class TesteContas {
	public static void main(String[] args) {
		
		ContaCorrente cc = new ContaCorrente(259, 72049);
		ContaPoupanca cp = new ContaPoupanca(533, 57095);
		
		cc.deposita(1000);
		cp.deposita(1000);
		
		cc.transfere(10, cp);
		System.out.println(cc.getSaldo());
		System.out.println(cp.getSaldo());
		
		
		cc.saca(590);
		
		System.out.println(cc.getSaldo());
	}
}
