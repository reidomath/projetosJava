
public abstract class Conta {
	private int agencia;
	private int conta;
	protected double saldo;
	private Cliente cliente = new Cliente();
	private static int totalContas;

	// Primeiro Construtor
	public Conta(int agencia, int conta) {
		this.agencia = agencia;
		this.conta = conta;
		//this.saldo = 3800;
		//System.out.println("Voc� acaba de criar uma conta! :)");
		Conta.totalContas++;
	}
	
	//Segundo Construtor
	public Conta(int agencia) {
		this(agencia, 344);		
	}

	public void setAgencia(int agencia) {
		if (agencia >= 0) {
			this.agencia = agencia;
		} else {
			System.out.println("Agencia n�o existe!");
		}
	}

	public int getAgencia() {
		return this.agencia;
	}

	public void setConta(int conta) {
		if (conta >= 0) {
			this.conta = conta;
		} else {
			System.out.println("N�o � possivel criar essa conta!");
		}
	}

	public int getConta() {
		return this.conta;
	}

	public boolean setSaldo(double saldo) {
		if (saldo <= 5400) {
			this.saldo = saldo;
			return true;
		} else {
			return false;
		}
	}

	public double getSaldo() {
		return this.saldo;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public static int getTotal() {
		return Conta.totalContas;
	}
	
	
	public boolean saca(double valor) {
		if (this.saldo >= valor) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
	
	public abstract void deposita(double valor);
	
	public boolean transfere(double valor, Conta destino) {
		if (this.saldo >= valor) {
			destino.saldo += valor;	
			return true;
		} else {
			return false;
		}
	}
}







