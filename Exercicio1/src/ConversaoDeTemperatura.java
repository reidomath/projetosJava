public class ConversaoDeTemperatura {
	public static String converteCparaF(double ceusius) {
		return "T� Celsius p/ Fahrenheit: " + String.format("%.2f", ((9.0 / 5.0) * ceusius + 32));
	}

	public static String converteCparaK(double ceusius) {
		return "T� Celsius p/ Kelvin: " + String.format("%.2f", (ceusius + 273.15));
	}

	public static String converteFparaC(double fahrenheit) {
		return "T� Fahrenheit p/ Celsius: " + String.format("%.2f", ((5.0 / 9.0) * (fahrenheit - 32)));
	}

	public static String converteKparaC(double kelvin) {
		return "T� Kelvin p/ Celsius: " + String.format("%.2f", (kelvin - 273.15));
	}

	public static String converteFparaK(double fahrenheit) {
		return "T� Fahrenheit p/ Kelvin: " + String.format("%.2f", ((5.0 / 9.0) * (fahrenheit + 459.67)));
	}

	public static String converteKparaF(double kelvin) {
		return "T� Kelvin p/ Fahrenheit: " + String.format("%.2f", ((9.0 / 5.0) * kelvin - 459.67));
	}
}
