import java.util.Scanner;

public class Teste {

	// Metodo que realiza a invoca��o da classe para convers�o, com entrada do
	// usuario
	public static void calculaTemp(int opcao, double temperatura) {
		switch (opcao) {
		case 1:
			System.out.println(ConversaoDeTemperatura.converteCparaF(temperatura));
			break;
		case 2:
			System.out.println(ConversaoDeTemperatura.converteCparaK(temperatura));
			break;
		case 3:
			System.out.println(ConversaoDeTemperatura.converteFparaC(temperatura));
			break;
		case 4:
			System.out.println(ConversaoDeTemperatura.converteKparaC(temperatura));
			break;
		case 5:
			System.out.println(ConversaoDeTemperatura.converteFparaK(temperatura));
			break;
		case 6:
			System.out.println(ConversaoDeTemperatura.converteKparaF(temperatura));
			break;
		default:
			System.out.println("Op��o indicada � invalida. Tente novamente!");
			break;
		}
	}

	public static void main(String[] args) {
		// Metodos Static n�o dependem de objetos
		Scanner scan = new Scanner(System.in);
		System.out.println("Digite um numero para realizar uma Convers�o:\n\n" + "(1) Celsius (C) para Fahrenheit (F)\n"
				+ "(2) Celsius (C) para Kelvin (K)\n" + "(3) Fahrenheit (F) para Celsius (C)\n"
				+ "(4) Kelvin (K) para Celsius (C)\n" + "(5) Fahrenheit (F) para Kelvin (K)\n"
				+ "(6) Kelvin (K) para Fahrenheit (F)");
		int opcao = scan.nextInt();
		System.out.println("\nAgora digite sua temperatura desejada: ");
		double temperatura = scan.nextDouble();
		scan.close();
		calculaTemp(opcao, temperatura);

	}
}
