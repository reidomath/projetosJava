
public class Gerente extends Funcionario implements Autenticavel {

	private AutenticaUtil autenticador;
	
	public Gerente() {
		this.autenticador = new AutenticaUtil();
	}
	
	
	public double getBonificacao() {
		System.out.println("Bonificação Gerente");
		//return super.getBonificacao() + super.getSalario();
		return this.salario + 300;
	}


	@Override
	public void setSenha(int senha) {
		this.autenticador.setSenha(senha);
		
	}

	@Override
	public boolean autentica(int senha) {
		return this.autentica(senha);
	}
}
