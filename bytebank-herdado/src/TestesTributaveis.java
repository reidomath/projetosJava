
public class TestesTributaveis {
	public static void main(String[] args) {
		ContaCorrente cc = new ContaCorrente();
		cc.deposita(100);
		
		
		SeguroDeVida sV = new SeguroDeVida();
		
		CalculadoDeImporto cI = new CalculadoDeImporto();
		
		cI.registra(cc);
		cI.registra(sV);
		
		
		System.out.println(cI.getTotalImposto());
	}
}
