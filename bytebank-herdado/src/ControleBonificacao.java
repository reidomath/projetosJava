
public class ControleBonificacao {

	private double soma;
	
	public double getSoma() {
		return this.soma;
	}
	
	public void registra (Funcionario funcionario) {
		double bonificacao = funcionario.getBonificacao();
		this.soma+=bonificacao;
	}
	
}
