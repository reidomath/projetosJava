
public class TesteFuncionario {
	public static void main(String[] args) {
		
		Funcionario matheusFerreira = new Gerente();
		
		matheusFerreira.setNome("Matheus Silva Ferreira");
		matheusFerreira.setCpf("433.194.368.21");
		matheusFerreira.setSalario(4680.00);
		
		System.out.println(matheusFerreira.getNome());
		System.out.println(matheusFerreira.getCpf());
		System.out.println(matheusFerreira.getSalario());
	}
}
