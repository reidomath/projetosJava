
public class Administrador extends Funcionario implements Autenticavel {

	private int senha;
	
	private AutenticaUtil util;
	
	public Administrador() {
		this.util = new AutenticaUtil();
	}

	@Override
	public double getBonificacao() {
		return 0;
	}

	@Override
	public void setSenha(int senha) {
		this.util.setSenha(senha);
		
	}

	@Override
	public boolean autentica(int senha) {
		return this.autentica(senha);
	}



}
