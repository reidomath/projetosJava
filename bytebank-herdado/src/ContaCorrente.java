
public class ContaCorrente extends Conta implements Tributavel{

	@Override
	public double getValorImposto() {
		return super.getSaldo() * 0.01;
	}

}
