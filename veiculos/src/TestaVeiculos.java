
public class TestaVeiculos {
	public static void main(String[] args) {
		Carro carro = new Carro();
		
		carro.andar(true);
		carro.setNome("MathMovel");
		System.out.println(carro.getNome());
		
		carro.andar();
		
		carro.abastecer();
		
		Aviao aviao = new Aviao();
		
		aviao.andar(false);
		
		aviao.voar();
		
		aviao.setTipoTransporte("balao");
		
		aviao.getTipoTransporte();
		
	}
}
