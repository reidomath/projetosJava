
public abstract class Veiculo {
	private String tipoTransporte;
	private int numPortas;
	private double valor;
	private String nome;
	
	public abstract void abastecer();
	
	public void andar(boolean anda) {
		if (anda == true) {
			System.out.println("Voce � um veiculo terrestre, portanto n�o voa!");
		} else {
			System.out.println("Voce � um veiculo aereo, portanto n�o anda!");
		}
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public int getNumPortas() {
		return numPortas;
	}

	public void setNumPortas(int numPortas) {
		this.numPortas = numPortas;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
