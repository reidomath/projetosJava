package aula1;

public class Lampada {
	private String marca;
	private String cor;
	private double frequencia;
	private boolean status;

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getCor() {
		return this.cor;
	}

	public void setFrequencia(double frequencia) {
		this.frequencia = frequencia;
	}

	public double getFrequencia() {
		return this.frequencia;
	}

	public void setStatus(boolean status) {
		this.status = status;
		if (this.status == true) {
			System.out.println("Voc� acendeu a lampada");
		} else {
			System.out.println("Voc� apagou a lampada");
		}
	}

	public boolean getStatus() {
		if (this.status == true) {
			System.out.println("Sua lampada encontra-se acesa!");
		} else {
			System.out.println("Sua lampada encontra-se apagada!");
		}
		return this.status;
	}
}
