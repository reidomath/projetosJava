package aula01;

public class ThreadDemo extends Thread {
	private String threadName;

	public ThreadDemo(String threadName) {
		this.threadName = threadName;
	}

	public void run() {
		try {
			for (int i = 4; i > 0; i--) {
				System.out.println("T:" + this.threadName + " " + i);
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {

		}
	}
}
