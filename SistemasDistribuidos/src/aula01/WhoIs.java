package aula01;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class WhoIs {
	public static void main(String[] args) {
		try {
			InetAddress mySelf = InetAddress.getLocalHost();

			System.out.println(mySelf.getHostName());
			System.out.println(mySelf.getHostAddress());
		} catch (UnknownHostException e) {
			System.out.println(e);
		}
	}
}
