
public class Assistente extends Funcionario{


	private int matricula;
	
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	
	public int getMatricula() {
		return this.matricula;
	}
	
	public String exibeDados() { 
		String dados = "Nome: " + getNome() + "\nRg: "+ getRg() + "\nIdade: "+ getIdade() + "\nSalario: " + getSalario(); 
		return dados;
	}
}
