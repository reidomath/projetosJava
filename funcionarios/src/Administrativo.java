
public class Administrativo extends Funcionario {
	private boolean turno; //dia = true && noite = false
	
	public void setTurno(boolean turno) {
		this.turno = turno;
	}
	
	public double getAdNoturno() {
		if (this.turno == true) {
			return 0;
		} else {
			return 200; 
		}
	}
}
