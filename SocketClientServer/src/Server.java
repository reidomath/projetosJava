import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	/*
	 * 1 - Criar servidor de conex�o 2 - Esperar por um pedido de Conex�o --- Outro
	 * processo 2.1 - Criar uma nova conex�o 3 - Criar Streams de entrada e saida 4
	 * - Tratar a conversa��o entre Cliente e Servidor (Tratar protocolo) 4.1 -
	 * Fechar socket de comunica��o entre Servidor / Cliente 4.2 Fechar Streams de
	 * entrada e saida 5 - Voltar para o passo 2 at� que finalize o programa 6 -
	 * Fechar serversocket
	 */

	private ServerSocket serverSocket;

	public void criarServerSocket(int porta) throws IOException {
		serverSocket = new ServerSocket(porta);
	}

	public Socket esperaConexao() throws IOException {
		Socket socket = serverSocket.accept();
		return socket;
	}

	public static void main(String[] args) {

		try {
		Server server = new Server();
		server.criarServerSocket(5555);
		Socket socket = server.esperaConexao();
		} catch (IOException e) {
			System.out.println("Falha na conex�o");
		}
	}
}
