
public class Conta {
	int agencia;
	int numeroConta;
	Cliente titular = new Cliente(); // Inst�ncia uma classe dentro de outra classe - REFERENCIA - assim como os
										// demais abaixo
	Endereco endereco = new Endereco();
	DetalhesConta detalhesConta = new DetalhesConta();

	public void localAgencia() {
		if (this.agencia == 1234) {
			System.out.println("Parab�ns, sua ag�ncia se situa a 3 km de sua resid�ncia atual");
		} else {
			System.out.println("Sua ag�ncia mais pr�xima � a cerca de 10 km de sua atual resid�ncia!");
		}
	}

}
