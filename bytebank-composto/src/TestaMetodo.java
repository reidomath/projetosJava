
public class TestaMetodo {
	public static void main(String[] args) {

		Conta math = new Conta();

		math.titular.nome = "Matheus da Silva Ferreira";
		math.titular.cpf = "433.194.368-21";
		System.out.println("Nome do titular: " + math.titular.nome);
		System.out.println("CPF do titular: " + math.titular.cpf);

		math.endereco.cep = "09210200";
		System.out.println("CEP do titular: " + math.endereco.cep);

		math.detalhesConta.dados.idade = 23;
		System.out.println(math.detalhesConta.dados.idade);

		char respostaDef = math.detalhesConta.dados.deficienciaFisica = 'N';
		math.detalhesConta.dados.testaDeficiencia(respostaDef);

		math.titular.profissao = "Desenvolvedor";

		Conta sumio = new Conta();

		sumio.agencia = 1234;
		sumio.localAgencia();

	}
}
