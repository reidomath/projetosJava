package tcpsocket;
import java.io.*;
import java.net.*;
public class TCPClient {
	public static void main(String args[]) throws Exception {
		Socket s = new Socket("127.0.0.1", 9000);
		OutputStream os = s.getOutputStream();
		DataOutputStream serverWriter = new DataOutputStream(os);
		InputStreamReader isrServer = new InputStreamReader(s.getInputStream());
		BufferedReader serverReader = new BufferedReader(isrServer);
		BufferedReader inFromUser = new BufferedReader(
				new InputStreamReader(System.in));
		String sentence;
		sentence = inFromUser.readLine();
		serverWriter.writeBytes(sentence + "\n");
		String response = serverReader.readLine();
		System.out.println(response);
		s.close();
	}
}