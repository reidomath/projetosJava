import java.util.Scanner;

class exercicio5 {
	// M�todos est�ticos s�o m�todos da classe, enquanto que m�todos n�o est�ticos
	// s�o do objeto
	
	//Metodo para ordenar o Vetor
	public static void ordenaVetor(int vetorAux[]) {
		int aux;
		for (int i = 0; i < vetorAux.length - 1; i++) {
			for (int j = 0; j < vetorAux.length - 1; j++) {
				// Verifica se o elemento da posi��o atual � maior que o elemento posterior
				if (vetorAux[j] > vetorAux[j + 1]) {
					//inverte a posi��o das variaveis no Vetor/
					aux = vetorAux[j + 1];
					vetorAux[j + 1] = vetorAux[j];
					vetorAux[j] = aux;
				}
			}
		}
		System.out.print("\n\nVetor ap�s da Ordena��o: ");
		for (int i = 0; i < vetorAux.length; i++) {
			System.out.print(vetorAux[i] + "  ");
		}
	}

	public static void main(String[] args) {

		System.out.println("Qual o tamanho do vetor? \n");
		// Scanner para ler tamanho do vetor desejado
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		scan.close();

		int vetor[] = new int[n];
		System.out.print("Vetor antes da Ordena��o: ");
		for (int i = 0; i < n; i++) {
			// Atribui um numero aleatorio a cada posi��o do Vetor
			vetor[i] = (int) (Math.random() * 100 + 1);
			System.out.print(vetor[i] + "  ");
		}

		// Invoca o metodo para ordena��o do vetor
		ordenaVetor(vetor);
	}
}
