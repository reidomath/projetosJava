import java.util.Scanner;

public class exercicio4 {

	public static int[] preencheVetor(int vetor[]) {
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = (int) (Math.random() * 10);
		}
		return vetor;
	}

	public static void insercaoVetor(int vetor[], int numero, int posicao) {
		boolean validaCheio = true, validaNaoPode = true, validaDireto = false;
		for (int i = 0; i < vetor.length; i++) {
			// Valida se h� pelo menos algum numero igual a '0' no vetor (Cheio)
			if (vetor[i] == 0) {
				validaCheio = false;
			}
			// Valida se as proximas posi��es depois da informada contem algum numero que
			// seja igual a '0' (N�o pode adicionar)
			if (i > posicao && vetor[i] == 0) {
				validaNaoPode = false;
			}
			// Se a posi��o informada conter '0', ja adiciona o numero informado na posi��o
			// (pode e contem zero na psoi��o informada)
			if (posicao == i && vetor[i] == 0) {
				vetor[i] = numero;
				validaDireto = true;
			}
		}
		// Valida se � possivel deslocar o vetor para adicionar o numero informado
		if (validaCheio == false && validaNaoPode == false && validaDireto == false) {
			for (int i = 0; i < vetor.length; i++) {
				// se a itera��o esta posterior a posi��o informada e o valor do vetor na
				// itera��o � igual a '0'
				if (i > posicao && vetor[i] == 0) {
					// Percorre o vetor entre a posi��o e o valor de '0', deslocando os valores
					for (int j = i; j > posicao; j--) {
						vetor[j] = vetor[j - 1];
					}
					// Atribui o numero ao vetor na posi��o informada
					vetor[posicao] = numero;
					break;
				}
			}

		}
		// Imprime os resultados obtidos, de acordo com o preenchimento do vetor
		if (validaCheio == true) {
			System.out.println("\nVetor cheio");
		} else if (validaNaoPode == true) {
			System.out.println("\nNumero n�o pode ser inserido no Vetor");
		} else {
			//Se o numero for inserido com sucesso, imprime o vetor ap�s inser��o
			System.out.print("\nVetor depois: ");
			for (int i = 0; i < vetor.length; i++) {
				System.out.print(vetor[i] + "  ");
			}
		}

	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Digite um numero para ser inserido no vetor:\n");
		int numero = scan.nextInt();
		System.out.println("Digite agora a posi��o para ser inserido:\n");
		int posicao = scan.nextInt();
		scan.close();

		int vetor[] = new int[10];
		// Invoca o metodo para preencher o vetor
		preencheVetor(vetor);
		System.out.print("\nVetor antes: ");
		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + "  ");
		}
		// Invoca o metodo para inserir o numero informado
		insercaoVetor(vetor, numero, posicao);
	}
}
