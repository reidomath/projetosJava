package carro;
import java.util.Scanner;

public class ProgramaPrincipal {
	public static void main(String[] args) {

		// Instanciando 3 classes do tipo Carro
		Carro c1 = new Carro();
		Carro c2 = new Carro();
		Carro c3 = new Carro();

		// Atribuindo valor aos atributos, atrav�s da invoca��o dos m�todos na classe
		// Carro
		c1.setConsumo(18.7);
		c1.addCarro(c1);
		c1.setModelo("Fiat Mobi");
		c1.setQtdadeCombustivel(30);
		c1.setTipoCombustivel("Gasolina");

		c2.setConsumo(19.2);
		c2.addCarro(c2);
		c2.setModelo("Jeep Renegade");
		c2.setQtdadeCombustivel(34);
		c2.setTipoCombustivel("Flex");

		c3.setConsumo(19);
		c3.addCarro(c3);
		c3.setModelo("Reneault Kwid");
		c3.setQtdadeCombustivel(22);
		c3.setTipoCombustivel("Alcool");

		// Printa o reusultado em tela
		System.out.println(c1.percorreDistancia(150));
		System.out.println(c1.getQtdadeCombustivel());
		System.out.println(c1.percorreDistancia(350));
		System.out.println(c1.getQtdadeCombustivel());

		// Solicita distancia ao usuario
		System.out.println("Digite uma distancia a ser percorrida: ");
		Scanner scan = new Scanner(System.in);
		double distancia = scan.nextDouble();
		scan.close();

		System.out.println("\nO carro mais economico �:");
		if (c1.getConsumo() > c2.getConsumo()) {
			if (c2.getConsumo() > c3.getConsumo()) {
				c3.imprimeDadosCarro();
				c3.percorreDistancia(distancia);
			} else {
				c2.imprimeDadosCarro();
				c2.percorreDistancia(distancia);
			}
		} else if (c2.getConsumo() > c1.getConsumo()) {
			if (c1.getConsumo() > c3.getConsumo()) {
				c3.imprimeDadosCarro();
				c3.percorreDistancia(distancia);
			} else {
				c1.imprimeDadosCarro();
				c1.percorreDistancia(distancia);
			}
		}
	}
}
