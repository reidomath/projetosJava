package carro;

public class Carro {

	private static int contador = 0;
	private Carro[] carro = new Carro[3];
	private String modelo;
	private String tipoCombustivel;
	private double qtdadeCombustivel;
	private double consumo;

	// Construtor da classe
	public Carro() {
		contador++;
	}

	public Carro addCarro(Carro carro) {
		return this.carro[contador-1] = carro;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getModelo() {
		return "Modelo: " + this.modelo;
	}

	public void setTipoCombustivel(String tipoCombustivel) {
		this.tipoCombustivel = tipoCombustivel;
	}

	public String getTipoCombustivel() {
		return "Tipo Combustivel: " + this.tipoCombustivel;
	}

	public void setQtdadeCombustivel(double qtdadeCombustivel) {
		this.qtdadeCombustivel += qtdadeCombustivel;
	}

	public double getQtdadeCombustivel() {
		return this.qtdadeCombustivel;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}

	public double getConsumo() {
		return this.consumo;
	}

	public double combustivelNecessario(double distancia) {
		return (distancia / this.consumo);
	}

	public int percorreDistancia(double distancia) {
		double necessario = (combustivelNecessario(distancia));
		if (this.qtdadeCombustivel > necessario) {
			this.qtdadeCombustivel -= necessario;
			System.out.println("Combustivel suficiente!");
			return 1;
		}
		System.out.println("Necessário abastecer!");
		return -1;
	}

	public void imprimeDadosCarro() {
		String imprime;
		imprime = "Modelo: " + this.modelo;
		imprime += "\nTipo Combustivel: " + this.tipoCombustivel;
		imprime += "\nQuantidade Combustivel: " + this.qtdadeCombustivel;
		imprime += "\nConsumo: " + this.consumo;
		System.out.println(imprime);
	}
	

}
