
public class exercicio2 {
	public static void main(String[] args) {
		int vetor[] = new int[20];
		double soma = 0;
		System.out.print("Vetor aleat�rio: ");
		for (int i = 0; i < 20; i++) {
			// Math.floor() para retirar a parte flutuante do numero aleatorio, * 10 at�
			// onde quero e +1 por qual come�a
			vetor[i] = (int) Math.floor(Math.random() * 10 + 1);
			System.out.print(vetor[i] + "  ");
			// Realiza a soma de todos os elementos do vetor a cada itera��o
			soma = soma + vetor[i];
		}

		System.out.println("\n- M�dia: " + soma / vetor.length);

		int moda = 0, aux, aux2 = 0, qtdade = 0;
		for (int j = 0; j < 20; j++) {
			qtdade = 0;
			aux = vetor[j];
			for (int w = 0; w < 20; w++) {
				// Verifica se o elemento fixado 'aux' possui mesmo valor que outras posi��es do
				// vetor
				if (aux == vetor[w]) {
					// 'qtdade' armazena a quantidade de vezes que o elemento se repete no array
					qtdade++;
				}
			}
			// Compara a moda do elemento atual (qtdade) com o elemento anterior (aux2)
			if (qtdade > aux2) {
				moda = vetor[j];
				aux2 = qtdade;
			}
		}
		System.out.println("- Moda: " + moda);
	}
}
