import java.util.Scanner;

public class exercicio1 {
	public static void main(String[] args) {
		System.out.println("Digite quantas parcelas deseja para soma infinita: \n");
		// Scanner para ler quantas parcelas
		Scanner scan = new Scanner(System.in);
		double parcelas = scan.nextInt();
		scan.close();

		double fracao = 4, aux = 1, sinal = -1;

		for (double i = 0; i < parcelas; i++) {
			// 'aux' para armazenar o valor do denominador da fra��o
			aux = aux + 2;
			// 'fracao' ira armazenar a soma de todas as parcelas
			fracao = fracao + ((sinal * 4) / aux);
			// 'sinal' varia entre 1 e -1 a cada itera��o do la�o
			sinal = sinal * -1;
		}
		System.out.println("\nO resultado da aproxima��o de pi �: " + fracao);
	}
}
