import java.util.Scanner;

public class exercicio3 {

	// M�todos est�ticos s�o m�todos da classe, enquanto que m�todos n�o est�ticos
	// s�o do objeto

	public static void preencheMatriz(int matrizAuxA[][], int matrizAuxB[][]) {
		if (matrizAuxA.length != matrizAuxB[0].length) {
			System.out.println("\n\nN�o foi possivel realizar o Produto das matrizes, dimens�es informadas s�o invalidas!");
		} else {
			// m.length determina o n�mero de linhas
			// m[i].length determina o n�mero de colunas
			int valida = 0;

			// Atribui uma matriz auxiliar para preencher a matrizA e matrizB
			int matriz[][] = matrizAuxA;
			String nomeMatriz = "\n\nMatriz A: \n";
			int matrizLinha = matrizAuxA.length;
			int matrizColuna = matrizAuxA[0].length;
			while (valida < 2) {
				for (int i = 0; i < matrizLinha; i++) {
					for (int j = 0; j < matrizColuna; j++) {
						// Atribui numero aleatorio na posi��o
						matriz[i][j] = (int) (Math.random() * 10 + 1);
					}
				}
				System.out.println(nomeMatriz);
				for (int i = 0; i < matriz.length; i++) {
					System.out.println();
					for (int j = 0; j < matriz[0].length; j++) {
						System.out.print(matriz[i][j] + "  ");
					}
				}
				// Repete a etapa anterior, agora para a MatrizB
				nomeMatriz = "\n\nMatriz B: \n";
				matriz = matrizAuxB;
				matrizLinha = matrizAuxB.length;
				matrizColuna = matrizAuxB[0].length;
				valida++;
			}
			// Invoca o m�todo para calcular o produto das Matrizes A e B
			produtoMatrizes(matrizAuxA, matrizAuxB);
		}
	}

	public static void produtoMatrizes(int matrizA[][], int matrizB[][]) {
		int matrizC[][] = null;
		// Verifica se as duas matrizes podem ser multiplicadas
		if (matrizA[0].length == matrizB.length) {
			// Inicializa a matriz resultante com a linha e coluna definida
			matrizC = new int[matrizA.length][matrizB[0].length];
			for (int i = 0; i < matrizA.length; i++)
				for (int j = 0; j < matrizB[0].length; j++) {
					int resultado = 0;
					for (int k = 0; k < matrizA[0].length; k++) {
						int produto = matrizA[i][k] * matrizB[k][j];
						// Variavel 'resultado' armazena o valor do produto iterando a cada
						// posi��o da linha x coluna da multiplica��o
						resultado += produto;
					}
					// Ap�s itera��o, valor da multiplica��o � atribuido para a posi��o 'ixj' da
					// Matriz
					matrizC[i][j] = resultado;
				}
		}
		// Se a multiplica��o for realizada, invoca o metodo para imprimir o resultado
		// da Matriz
		imprimeMatriz(matrizC);
	}

	public static void imprimeMatriz(int matrizC[][]) {
		System.out.println("\n\nResultado Multiplica��o Matriz A x Matriz B:");
		for (int i = 0; i < matrizC.length; i++) {
			System.out.println("\n");
			for (int j = 0; j < matrizC[0].length; j++) {
				System.out.print(matrizC[i][j] + "   ");
			}
		}
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Digite a dimens�o da Matriz A (linha e coluna): \n");
		int a = scan.nextInt();
		int b = scan.nextInt();
		int matrizA[][] = new int[a][b];

		System.out.println("\nDigite a dimens�o da Matriz B (linha e coluna): \n");
		int c = scan.nextInt();
		int d = scan.nextInt();
		int matrizB[][] = new int[c][d];
		scan.close();

		// Invoca o metodo para Preenchimento da MatrizA e matrizB
		preencheMatriz(matrizA, matrizB);

	}

}
