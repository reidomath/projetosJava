
public class Funcionario {
	String nome;
	int idade;
	double altura;
	int peso;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;

		if (true) {
			System.out.println(this.nome + " � linda!");
		}
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public void testeAltura(double d) {
		this.altura = d;
		
		if (this.altura < 1.70) {
			System.out.println(":D");
		} else {
			System.out.println(":(");
		}

		System.out.println(this.nome + " Sua altura �: " + this.altura + ":D");

	}

}
